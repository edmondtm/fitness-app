// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCvLQO3zGrUynffS-yjLkTPKrW7s01vMnY",
    authDomain: "fitness-app-26a67.firebaseapp.com",
    databaseURL: "https://fitness-app-26a67.firebaseio.com",
    projectId: "fitness-app-26a67",
    storageBucket: "fitness-app-26a67.appspot.com",
    messagingSenderId: "798340048982"
  }
};